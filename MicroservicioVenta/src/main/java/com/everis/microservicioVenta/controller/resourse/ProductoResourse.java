package com.everis.microservicioVenta.controller.resourse;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ProductoResourse {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long idProducto;
	@Column
	private String descripcion;
	@Column
	private String nombre;
	@Column
	private String unidadMedida;

}
