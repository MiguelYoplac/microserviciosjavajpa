package com.everis.microservicioOrden.model.entity;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Orden {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long idOrden;
	@ManyToOne
	@JoinColumn(name="idVendedor")
	private Vendedor vendedor;
	@ManyToOne
	@JoinColumn(name="idCliente")
	private Cliente cliente;
	@Column
	private String fechaOrden;
	@Column
	private BigDecimal total;
	@Column
	private BigDecimal igv;
}
