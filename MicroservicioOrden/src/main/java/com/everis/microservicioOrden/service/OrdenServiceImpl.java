package com.everis.microservicioOrden.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.everis.microservicioOrden.model.entity.DetalleOrden;
import com.everis.microservicioOrden.model.entity.Orden;
import com.everis.microservicioOrden.model.repository.DetalleOrdenRepository;
import com.everis.microservicioOrden.model.repository.OrdenRepository;

@Service
public class OrdenServiceImpl implements OrdenService {
	
	//ORDEN
	
	@Autowired
	private OrdenRepository ordenRepository;
	
	@Override
	public Iterable<Orden> obtenerOrdenes(){
		return ordenRepository.findAll();
	}

	@Override
	public Orden insertarOrden(Orden orden) throws Exception {
		return ordenRepository.save(orden);
	}
	
	@Override
	public Orden obtenerOrdenPorId(long id) throws Exception {
		return ordenRepository.findById(id).orElseThrow(()->new Exception("Orden no Encontrada"));
	}
	
	//DETALLE ORDEN
	
	@Autowired
	private DetalleOrdenRepository detalleOrdenRepository;
		
	@Override
	public Iterable<DetalleOrden> obtenerDetalleOrden(){
		return detalleOrdenRepository.findAll();
	}

	@Override
	public DetalleOrden insertarDetalleOrden(DetalleOrden detalleOrden) throws Exception {
		return detalleOrdenRepository.save(detalleOrden);
	}
	
	@Override
	public DetalleOrden obtenerDetalleOrdenPorId(long id) throws Exception{
		return detalleOrdenRepository.findById(id).orElseThrow(()->new Exception("Detalle Orden no Encontrada"));
	}


}
