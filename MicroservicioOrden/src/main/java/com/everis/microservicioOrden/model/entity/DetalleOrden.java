package com.everis.microservicioOrden.model.entity;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class DetalleOrden {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long idDetalleOrden;
	@ManyToOne
	@JoinColumn(name="idVendedorProducto")
	private VendedorProducto vendedorProducto;
	@ManyToOne
	@JoinColumn(name="idOrden")
	private Orden orden;
	@Column
	private Integer cantidad;
	@Column
	private BigDecimal precioUnitario;
}
