package com.everis.microservicioProducto.controller.resource;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ProductoReducidoResource {

	private String descripcion;
	private String nombre;
	private String unidadMedida;
}
