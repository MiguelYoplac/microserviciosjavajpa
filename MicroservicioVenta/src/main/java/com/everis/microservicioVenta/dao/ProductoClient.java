package com.everis.microservicioVenta.dao;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;

import com.everis.microservicioVenta.controller.resourse.ProductoResourse;
//import com.everis.microservicioVenta.model.entity.Producto;


@FeignClient("MICROSERVICIOPRODUCTO")
public interface ProductoClient {

	@GetMapping("/productos/{id}")
	public ProductoResourse obtenerProductoPorId(@PathVariable("id") long id) throws Exception;
	
	@GetMapping("/productos")
	public List<ProductoResourse> obtenerProductos();
}