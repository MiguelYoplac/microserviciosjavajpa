package com.everis.microservicioOrden.service;

import com.everis.microservicioOrden.model.entity.DetalleOrden;
import com.everis.microservicioOrden.model.entity.Orden;

public interface OrdenService {
	
	//LISTAR ORDEN
	
	public Iterable<Orden> obtenerOrdenes();
	
	//GUARDAR ORDEN
	
	public Orden insertarOrden(Orden Orden) throws Exception;
	
	//OBTENER VENDEDORPRODUCTO
	
	public Orden obtenerOrdenPorId(long id) throws Exception;
	
	//LISTAR DETALLE ORDEN
	
	public Iterable<DetalleOrden> obtenerDetalleOrden();
		
	//GUARDAR DETALLE ORDEN
		
	public DetalleOrden insertarDetalleOrden(DetalleOrden detalleOrden) throws Exception;
	
	//OBTENER DETALLE ORDEN
	
	public DetalleOrden obtenerDetalleOrdenPorId(long id) throws Exception;
}
