package com.everis.microservicioCliente.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.everis.microservicioCliente.controller.resourse.ClienteReducidoResourse;
import com.everis.microservicioCliente.controller.resourse.ClienteResourse;
import com.everis.microservicioCliente.model.entity.Cliente;
import com.everis.microservicioCliente.service.ClienteService;

@RestController
public class ApiController {

	@Autowired
	@Qualifier("clienteServiceImpl")
	ClienteService clienteService;
	
	@GetMapping("/clientes")
	public List<ClienteResourse> obtenerClientes() {
		
		List<ClienteResourse> listado = new ArrayList<>();
		clienteService.obtenerClientes().forEach(cliente -> {
			ClienteResourse clienteResourse = new ClienteResourse();
			clienteResourse.setIdCliente(cliente.getIdCliente());
			clienteResourse.setNombres(cliente.getNombres());
			clienteResourse.setApellidos(cliente.getApellidos());
			clienteResourse.setDirección(cliente.getDirección());
			listado.add(clienteResourse);
		});
		return listado;
	}
	
	@GetMapping("/clientes/{id}")
	public ClienteResourse obtenerClientePorId(@PathVariable("id") long id) throws Exception {
		Cliente cliente = clienteService.obtenerClientePorId(id);
		ClienteResourse clienteResourse = new ClienteResourse();
		clienteResourse.setIdCliente(cliente.getIdCliente());
		clienteResourse.setNombres(cliente.getNombres());
		clienteResourse.setApellidos(cliente.getApellidos());
		clienteResourse.setDirección(cliente.getDirección());
		return clienteResourse;
	}
	
	@PostMapping("/clientes")
	public ResponseEntity<ClienteResourse> guardarCliente(@RequestBody ClienteReducidoResourse request) throws Exception {	
		Cliente cliente = new Cliente();
		cliente.setNombres(request.getNombres());
		cliente.setApellidos(request.getApellidos());
		cliente.setDirección(request.getDirección());	
		Cliente clienteNuevo = clienteService.insertarCliente(cliente);
		ClienteResourse clienteResourse = new ClienteResourse();
		clienteResourse.setIdCliente(clienteNuevo.getIdCliente());
		clienteResourse.setNombres(clienteNuevo.getNombres());
		clienteResourse.setApellidos(clienteNuevo.getApellidos());
		clienteResourse.setDirección(clienteNuevo.getDirección());
		return new ResponseEntity<>(clienteResourse, HttpStatus.CREATED);
	}
}
