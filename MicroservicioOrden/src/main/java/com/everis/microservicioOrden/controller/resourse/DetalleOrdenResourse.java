package com.everis.microservicioOrden.controller.resourse;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.everis.microservicioOrden.model.entity.Orden;
import com.everis.microservicioOrden.model.entity.VendedorProducto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class DetalleOrdenResourse {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long idDetalleOrden;
	@ManyToOne
	@JoinColumn(name="idVendedorProducto")
	private VendedorProducto vendedorProducto;
	@ManyToOne
	@JoinColumn(name="idOrden")
	private Orden orden;
	@Column
	private Integer cantidad;
	@Column
	private BigDecimal precioUnitario;
}
