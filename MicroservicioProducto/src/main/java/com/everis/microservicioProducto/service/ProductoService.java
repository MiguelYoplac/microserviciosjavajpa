package com.everis.microservicioProducto.service;

import com.everis.microservicioProducto.model.entity.Producto;

public interface ProductoService {
	
	//LISTAR
	
	public Iterable<Producto> obtenerProductos();
	
	//GUARDAR
	
	public Producto insertarProducto(Producto producto) throws Exception;
	
	//OBTENER
	
	public Producto obtenerProductoPorId(long id) throws Exception;
	
}
