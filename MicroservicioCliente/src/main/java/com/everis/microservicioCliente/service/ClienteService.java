package com.everis.microservicioCliente.service;

import com.everis.microservicioCliente.model.entity.Cliente;

public interface ClienteService {
	
	//LISTAR
	
	public Iterable<Cliente> obtenerClientes();
	
	//GUARDAR
	
	public Cliente insertarCliente(Cliente producto) throws Exception;
	
	//OBTENER
	
	public Cliente obtenerClientePorId(long id) throws Exception;
	
}
