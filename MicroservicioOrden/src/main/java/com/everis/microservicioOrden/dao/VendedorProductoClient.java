package com.everis.microservicioOrden.dao;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;

import com.everis.microservicioOrden.controller.resourse.VendedorProductoResourse;
import com.everis.microservicioOrden.controller.resourse.VendedorResourse;

@FeignClient("MICROSERVICIOVENTA")
public interface VendedorProductoClient {
	
	//VENDEDOR PRODUCTO

	@GetMapping("/vendedorProducto/{id}")
	public VendedorProductoResourse obtenerVendedorProductoPorId(@PathVariable("id") long id) throws Exception;
	
	@GetMapping("/vendedorProducto")
	public List<VendedorProductoResourse> obtenerProductos();
	
	//VENDEDOR
	
	@GetMapping("/vendedores/{id}")
	public VendedorResourse obtenerVendedorPorId(@PathVariable("id") long id) throws Exception;
	
	@GetMapping("/vendedores")
	public List<VendedorResourse> obtenerVendedores();
}