package com.everis.microservicioCliente.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.everis.microservicioCliente.model.entity.Cliente;
import com.everis.microservicioCliente.model.repository.ClienteRepository;

@Service
public class ClienteServiceImpl implements ClienteService {
	
	@Autowired
	private ClienteRepository clienteRepository;
	
	@Override
	public Iterable<Cliente> obtenerClientes(){
		return clienteRepository.findAll();
	}

	@Override
	public Cliente insertarCliente(Cliente cliente) throws Exception {
		return clienteRepository.save(cliente);
	}
	
	@Override
	public Cliente obtenerClientePorId(long id) throws Exception {
		return clienteRepository.findById(id).orElseThrow(()->new Exception("Cliente no Encontrado"));
	}
}
