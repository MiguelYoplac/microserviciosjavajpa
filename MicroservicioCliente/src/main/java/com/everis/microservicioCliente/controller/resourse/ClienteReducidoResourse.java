package com.everis.microservicioCliente.controller.resourse;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ClienteReducidoResourse {
	
	private String nombres;

	private String apellidos;

	private String dirección;
}
