package com.everis.microservicioOrden.controller.resourse;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.everis.microservicioOrden.model.entity.Cliente;
import com.everis.microservicioOrden.model.entity.Vendedor;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrdenResourse {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long idOrden;
	@ManyToOne
	@JoinColumn(name="idVendedor")
	private Vendedor vendedor;
	@ManyToOne
	@JoinColumn(name="idCliente")
	private Cliente cliente;
	@Column
	private String fechaOrden = new SimpleDateFormat("dd-MM-yyy").format(new Date());
	@Column
	private BigDecimal total;
	@Column
	private BigDecimal igv;

}
