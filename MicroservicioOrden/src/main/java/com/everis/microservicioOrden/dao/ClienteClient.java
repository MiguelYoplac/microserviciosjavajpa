package com.everis.microservicioOrden.dao;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.everis.microservicioOrden.controller.resourse.ClienteResourse;

@FeignClient("MICROSERVICIOCLIENTE")
public interface ClienteClient {

	@GetMapping("/clientes/{id}")
	public ClienteResourse obtenerClientePorId(@PathVariable("id") long id) throws Exception;
	
	@GetMapping("/clientes")
	public List<ClienteResourse> obtenerClientes();
}