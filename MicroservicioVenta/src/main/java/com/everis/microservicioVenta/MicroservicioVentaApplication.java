package com.everis.microservicioVenta;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients//("com.everis")
@EnableDiscoveryClient
public class MicroservicioVentaApplication {

	public static void main(String[] args) {
		SpringApplication.run(MicroservicioVentaApplication.class, args);
	}

}
