package com.everis.microservicioVenta.service;

import com.everis.microservicioVenta.model.entity.Producto;
import com.everis.microservicioVenta.model.entity.Vendedor;
import com.everis.microservicioVenta.model.entity.VendedorProducto;

public interface VendedorService {
	
	//LISTAR VENDEDOR
	
	public Iterable<Vendedor> obtenerVendedores();
	
	//GUARDAR VENDEDOR
	
	public Vendedor insertarVendedor(Vendedor producto) throws Exception;
	
	//OBTENER VENDEDOR
	
	public Vendedor obtenerVendedorPorId(long id) throws Exception;
	
	//LISTAR VENDEDORPRODUCTO
	
	public Iterable<VendedorProducto> obtenerVendedorProducto();
		
	//GUARDAR VENDEDORPRODUCTO
		
	public VendedorProducto insertarVendedorProducto(VendedorProducto producto) throws Exception;
		
	//OBTENER VENDEDORPRODUCTO
	
	public VendedorProducto obtenerVendedorProductoPorId(long id) throws Exception;
	
	//OBTENER PRODUCTO
	
	public Producto obtenerProductoPorId(long idProducto)throws Exception ;
	
}
