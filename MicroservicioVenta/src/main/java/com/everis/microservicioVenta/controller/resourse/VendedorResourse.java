package com.everis.microservicioVenta.controller.resourse;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class VendedorResourse {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long idVendedor;
	@Column
	private String nombres;
	@Column
	private String apellidos;
	@Column
	private String cargo;
}
