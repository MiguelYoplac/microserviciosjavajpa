servers:
	-Venta:8085
	-Producto: 8088
	-Orden:8086
	-Cliente:8087

Datos para Orden:

{
	"idVendedor": "1",
	"idCliente": "1",
	"total": 10.5,
	"igv": 2.5
}

Datos para Vendedor

{
   	"nombres": "Jimena",
    	"apellidos": "Jimenez",
   	"cargo": "CEO"
}

Datos para DetalleOrden

{
	"idVendedorProducto": "3",
	"idOrden": "10",
	"cantidad": 10,
	"precioUnitario": 2.5
}

Datos para Producto

{
	"nombre": "Arroz",
	"descripcion": "Arroz de 5kg",
	"unidadMedida": "Kilos"
}

Datos para Cliente

{
	"nombres": "Miguel",
	"apellidos": "Yoplac",
	"dirección": "Las manzanitas 123"
}

Datos para VendedorProducto

{
	"idVendedorProducto": "3",
	"idOrden": "10",
	"cantidad": 10,
	"precioUnitario": 2.5
}