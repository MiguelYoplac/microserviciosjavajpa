package com.everis.microservicioVenta.model.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.everis.microservicioVenta.model.entity.Vendedor;

@Repository
public interface VendedorRepository extends CrudRepository<Vendedor, Long> {

}
