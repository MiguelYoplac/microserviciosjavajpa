package com.everis.microservicioOrden.controller;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.everis.microservicioOrden.controller.resourse.ClienteResourse;
import com.everis.microservicioOrden.controller.resourse.DetalleOrdenResourse;
import com.everis.microservicioOrden.controller.resourse.OrdenReducidoResourse;
import com.everis.microservicioOrden.controller.resourse.OrdenResourse;
import com.everis.microservicioOrden.controller.resourse.VendedorProductoResourse;
import com.everis.microservicioOrden.controller.resourse.VendedorResourse;
import com.everis.microservicioOrden.dao.ClienteClient;
import com.everis.microservicioOrden.dao.VendedorProductoClient;
import com.everis.microservicioOrden.model.entity.Cliente;
import com.everis.microservicioOrden.model.entity.DetalleOrden;
import com.everis.microservicioOrden.model.entity.Orden;
import com.everis.microservicioOrden.model.entity.Producto;
import com.everis.microservicioOrden.model.entity.Vendedor;
import com.everis.microservicioOrden.model.entity.VendedorProducto;
import com.everis.microservicioOrden.service.OrdenService;

@RestController
public class ApiController {
	
	@Value("${igv}")
	BigDecimal igv;
	
	@Autowired
	@Qualifier("ordenServiceImpl")
	OrdenService ordenService;
	
	//ORDEN------------------------------------------------------------------------
	
	//Obtener el cliente por feign
	@Autowired
	private ClienteClient clienteClient;
	
	@PostMapping("/orden")
	public ResponseEntity<DetalleOrdenResourse> guardarOrden(@RequestBody OrdenReducidoResourse request) throws Exception {	
		
		Orden orden = new Orden();
		DetalleOrden detalleOrden = new DetalleOrden();
		
		//OBTENER CLIENTE
		Cliente cliente = new Cliente();
		ClienteResourse clienteResourse = clienteClient.obtenerClientePorId(request.getIdCliente());
		cliente.setIdCliente(clienteResourse.getIdCliente());
		cliente.setNombres(clienteResourse.getNombres());
		cliente.setApellidos(clienteResourse.getApellidos());
		cliente.setDirección(clienteResourse.getDirección());
		orden.setCliente(cliente); //LLENAR CLIENTE EN ORDEN
		
		//OBTENER VENDEDOR
		Vendedor vendedor = new Vendedor();
		VendedorResourse vendedorResourse = vendedorProductoClient.obtenerVendedorPorId(request.getIdVendedor());
		vendedor.setIdVendedor(vendedorResourse.getIdVendedor());
		vendedor.setNombres(vendedorResourse.getNombres());
		vendedor.setApellidos(vendedorResourse.getApellidos());
		vendedor.setCargo(vendedorResourse.getCargo());
		orden.setVendedor(vendedor); //LLENAR VENDEDOR EN ORDEN
		
		//OBTENER VENDEDOR PRODUCTO
		VendedorProducto vendedorProducto = new VendedorProducto();
		VendedorProductoResourse vendedorProductoResourse = vendedorProductoClient.obtenerVendedorProductoPorId(request.getIdVendedorProducto());
		vendedorProducto.setIdVendedorProducto(vendedorProductoResourse.getIdVendedorProducto());
		Producto producto = new Producto(
				vendedorProductoResourse.getProducto().getIdProducto(),
				vendedorProductoResourse.getProducto().getNombre(),
				vendedorProductoResourse.getProducto().getDescripcion(),
				vendedorProductoResourse.getProducto().getUnidadMedida());
		vendedorProducto.setProducto(producto);
		vendedorProducto.setVendedor(vendedorProductoResourse.getVendedor());
		vendedorProducto.setPrecio(vendedorProductoResourse.getPrecio());
		detalleOrden.setVendedorProducto(vendedorProducto); //LLENAR VENDEDORPRODUCTO EN DETALLE ORDEN
			
		//CÁLCULOS
		BigDecimal cantidad = new BigDecimal(request.getCantidad());
		BigDecimal pBruto = vendedorProductoResourse.getPrecio().multiply(cantidad);
		BigDecimal pNeto = pBruto.add(pBruto.multiply(igv));
		
		//LLENAR DATOS FALTANTES
		orden.setIgv(pBruto.multiply(igv)); //LLENAR IGV EN ORDEN
		orden.setTotal(pNeto); //LLENAR TOTAL EN ORDEN
		orden.setFechaOrden(new SimpleDateFormat("dd-MM-yyy").format(new Date())); //LENAR FECHA EN ORDEN	
		detalleOrden.setOrden(orden); //LLENAR ORDEN EN DETALLE ORDEN
		detalleOrden.setCantidad(request.getCantidad()); //LLENAR CANTIDAD EN DETALLE ORDEN
		detalleOrden.setPrecioUnitario(vendedorProductoResourse.getPrecio()); //LLENAR PRECIO EN DETALLE ORDEN
		
		//INSERTAR ORDEN
		Orden ordenNuevo = ordenService.insertarOrden(orden);	
		
		//INSERTAR DETALLE ORDEN
		detalleOrden.setIdDetalleOrden(ordenNuevo.getIdOrden());
		DetalleOrden detalleOrdenNuevo = ordenService.insertarDetalleOrden(detalleOrden);
				
		ModelMapper modelMapper = new ModelMapper();
		DetalleOrdenResourse detalleOrdenResourse = modelMapper.map(detalleOrdenNuevo, DetalleOrdenResourse.class);

		return new ResponseEntity<>(detalleOrdenResourse, HttpStatus.CREATED);
	}
	
	@GetMapping("/orden")
	public List<OrdenResourse> obtenerVendedorProducto() {
		
		List<OrdenResourse> listado = new ArrayList<>();
		ordenService.obtenerOrdenes().forEach(orden -> {
			OrdenResourse ordenResourse = new OrdenResourse();
			ordenResourse.setIdOrden(orden.getIdOrden());
			
			//CLIENTE
			ModelMapper modelMapper = new ModelMapper();
			Cliente cliente = modelMapper.map(orden.getCliente(), Cliente.class);

			//VENDEDOR
			Vendedor vendedor = new Vendedor();
			vendedor.setIdVendedor(orden.getVendedor().getIdVendedor());
			vendedor.setNombres(orden.getVendedor().getNombres());
			vendedor.setApellidos(orden.getVendedor().getApellidos());
			vendedor.setCargo(orden.getVendedor().getCargo());
			
			ordenResourse.setCliente(cliente);
			ordenResourse.setVendedor(vendedor);
			ordenResourse.setFechaOrden(orden.getFechaOrden());
			ordenResourse.setTotal(orden.getTotal());
			ordenResourse.setIgv(orden.getIgv());
			listado.add(ordenResourse);	
		});
		return listado;
	}
	
	//ORDEN DETALLE------------------------------------------------------------------------
	//Obtener vendedorProducto por feign
		
		@Autowired
		private VendedorProductoClient vendedorProductoClient;
		
		@GetMapping("/ordenDetalle")
		public List<DetalleOrdenResourse> obtenerDetalleOrden() {
			
			List<DetalleOrdenResourse> listado = new ArrayList<>();
			ordenService.obtenerDetalleOrden().forEach(detalleOrden -> {
				DetalleOrdenResourse detalleOrdenResourse = new DetalleOrdenResourse();
				detalleOrdenResourse.setIdDetalleOrden(detalleOrden.getIdDetalleOrden());
				detalleOrdenResourse.setOrden(detalleOrden.getOrden());
				detalleOrdenResourse.setVendedorProducto(detalleOrden.getVendedorProducto());
				detalleOrdenResourse.setCantidad(detalleOrden.getCantidad());
				detalleOrdenResourse.setPrecioUnitario(detalleOrden.getPrecioUnitario());				
				listado.add(detalleOrdenResourse);	
			});
			return listado;
		}
		
		@GetMapping("/ordenDetalle/{id}")
		public DetalleOrdenResourse obtenerDetalleOrdenPorId(@PathVariable("id") long id) throws Exception {
			DetalleOrden detalleOrden = ordenService.obtenerDetalleOrdenPorId(id);
			DetalleOrdenResourse detalleOrdenResourse = new ModelMapper().map(detalleOrden, DetalleOrdenResourse.class);
			return detalleOrdenResourse;
		}
}
