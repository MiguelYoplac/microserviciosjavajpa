package com.everis.microservicioOrden.model.entity;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="vendedorProducto")
public class VendedorProducto {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long idVendedorProducto;
	@ManyToOne
	@JoinColumn(name="idVendedor")
	private Vendedor vendedor;
	@ManyToOne
	@JoinColumn(name="idProducto")
	private Producto producto;
	@Column
	private BigDecimal precio;
}
