package com.everis.microservicioProducto.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.everis.microservicioProducto.controller.resource.ProductoReducidoResource;
import com.everis.microservicioProducto.controller.resource.ProductoResource;
import com.everis.microservicioProducto.model.entity.Producto;
import com.everis.microservicioProducto.service.ProductoService;

@RestController
public class ApiController {

	@Autowired
	@Qualifier("productoServiceImpl")
	ProductoService productoService;
	
	@GetMapping("/productos")
	public List<ProductoResource> obtenerProductos() {
		
		List<ProductoResource> listado = new ArrayList<>();
		productoService.obtenerProductos().forEach(producto -> {
			ProductoResource productoResource = new ProductoResource();
			productoResource.setIdProducto(producto.getIdProducto());
			productoResource.setNombre(producto.getNombre());
			productoResource.setDescripcion(producto.getDescripcion());
			productoResource.setUnidadMedida(producto.getUnidadMedida());
			listado.add(productoResource);
		});
		return listado;
	}
	
	@GetMapping("/productos/{id}")
	public ProductoResource obtenerProductoPorId(@PathVariable("id") long id) throws Exception {
		Producto producto = productoService.obtenerProductoPorId(id);
		ProductoResource productoResource = new ProductoResource();
		productoResource.setIdProducto(producto.getIdProducto());
		productoResource.setNombre(producto.getNombre());
		productoResource.setDescripcion(producto.getDescripcion());
		productoResource.setUnidadMedida(producto.getUnidadMedida());
		return productoResource;
	}
	
	@PostMapping("/productos")
	public ResponseEntity<ProductoResource> guardarProducto(@RequestBody ProductoReducidoResource request) throws Exception {	
		Producto producto = new Producto();
		producto.setNombre(request.getNombre());
		producto.setDescripcion(request.getDescripcion());
		producto.setUnidadMedida(request.getUnidadMedida());	
		Producto productoNuevo = productoService.insertarProducto(producto);
		ProductoResource productoResource = new ProductoResource();
		productoResource.setIdProducto(productoNuevo.getIdProducto());
		productoResource.setNombre(productoNuevo.getNombre());
		productoResource.setDescripcion(productoNuevo.getDescripcion());
		productoResource.setUnidadMedida(productoNuevo.getUnidadMedida());
		return new ResponseEntity<>(productoResource, HttpStatus.CREATED);
	}

}
