package com.everis.microservicioVenta.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Vendedor {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long idVendedor;
	@Column
	private String nombres;
	@Column
	private String apellidos;
	@Column
	private String cargo;

}