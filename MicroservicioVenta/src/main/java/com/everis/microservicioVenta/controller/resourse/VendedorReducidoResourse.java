package com.everis.microservicioVenta.controller.resourse;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class VendedorReducidoResourse {

	private Long idVendedor;
	private String nombres;
	private String apellidos;
	private String cargo;
}
