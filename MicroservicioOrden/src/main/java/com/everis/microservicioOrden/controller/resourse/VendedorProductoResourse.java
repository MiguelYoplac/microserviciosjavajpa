package com.everis.microservicioOrden.controller.resourse;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.everis.microservicioOrden.model.entity.Producto;
import com.everis.microservicioOrden.model.entity.Vendedor;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class VendedorProductoResourse {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idVendedorProducto;
	@ManyToOne
	@JoinColumn(name="idVendedor")
	private Vendedor vendedor;
	@ManyToOne
	@JoinColumn(name="idProducto")
	private Producto producto;
	@Column
	private BigDecimal precio;
}
