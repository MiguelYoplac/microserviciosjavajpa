package com.everis.microservicioVenta.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.everis.microservicioVenta.controller.resourse.ProductoResourse;
import com.everis.microservicioVenta.dao.ProductoClient;
import com.everis.microservicioVenta.model.entity.Producto;
import com.everis.microservicioVenta.model.entity.Vendedor;
import com.everis.microservicioVenta.model.entity.VendedorProducto;
import com.everis.microservicioVenta.model.repository.VendedorProductoRepository;
import com.everis.microservicioVenta.model.repository.VendedorRepository;

@Service
public class VendedorServiceImpl implements VendedorService {
	
	//VENDEDOR
	
	@Autowired
	private VendedorRepository vendedorRepository;
	
	@Override
	public Iterable<Vendedor> obtenerVendedores(){
		return vendedorRepository.findAll();
	}

	@Override
	public Vendedor insertarVendedor(Vendedor vendedor) throws Exception {
		return vendedorRepository.save(vendedor);
	}
	
	@Override
	public Vendedor obtenerVendedorPorId(long id) throws Exception {
		return vendedorRepository.findById(id).orElseThrow(()->new Exception("Vendedor no Encontrado"));
	}
	
	//VENDEDOR PRODUCTO
	
	@Autowired
	private VendedorProductoRepository vendedorProductoRepository;
	
	@Override
	public Iterable<VendedorProducto> obtenerVendedorProducto(){
		return vendedorProductoRepository.findAll();
	}

	@Override
	public VendedorProducto insertarVendedorProducto(VendedorProducto vendedorProducto) throws Exception {
		return vendedorProductoRepository.save(vendedorProducto);
	}
	
	@Override
	public VendedorProducto obtenerVendedorProductoPorId(long id) throws Exception {
		return vendedorProductoRepository.findById(id).orElseThrow(()->new Exception("VendedorProducto no Encontrado"));
	}

	//PRODUCTO
	
	//Obtener el producto por feign
	@Autowired
	private ProductoClient productoClient;
	
	@Override
	public Producto obtenerProductoPorId(long idProducto) throws Exception {
		Producto producto = new Producto();
		ProductoResourse productoResourse = productoClient.obtenerProductoPorId(idProducto);
		producto.setIdProducto(productoResourse.getIdProducto());
		producto.setNombre(productoResourse.getNombre());
		producto.setDescripcion(productoResourse.getDescripcion());
		producto.setUnidadMedida(productoResourse.getUnidadMedida());
		return producto;
	}
}
