package com.everis.microservicioOrden.model.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.everis.microservicioOrden.model.entity.Orden;

@Repository
public interface OrdenRepository extends CrudRepository<Orden, Long> {

}
