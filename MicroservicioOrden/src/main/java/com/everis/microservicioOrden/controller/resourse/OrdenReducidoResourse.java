package com.everis.microservicioOrden.controller.resourse;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class OrdenReducidoResourse {

	private Long idVendedor;
	private Long idCliente;
	private Long idVendedorProducto;
	private Integer cantidad;
}
