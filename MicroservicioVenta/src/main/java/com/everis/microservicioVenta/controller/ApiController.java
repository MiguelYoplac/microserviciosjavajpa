package com.everis.microservicioVenta.controller;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.everis.microservicioVenta.controller.resourse.VendedorProductoReducidoResourse;
import com.everis.microservicioVenta.controller.resourse.VendedorProductoResourse;
import com.everis.microservicioVenta.controller.resourse.VendedorReducidoResourse;
import com.everis.microservicioVenta.controller.resourse.VendedorResourse;
import com.everis.microservicioVenta.model.entity.Producto;
import com.everis.microservicioVenta.model.entity.Vendedor;
import com.everis.microservicioVenta.model.entity.VendedorProducto;
import com.everis.microservicioVenta.service.VendedorService;

@RestController
public class ApiController {
	
//	@PostMapping("/feign/productos/")
//	public ProductoResourse getCountryUsingFeign(@PathVariable long idPorducto) throws Exception {
//		ProductoResourse response = productoClient.obtenerProductoPorId(idPorducto);     
//	    return response;
//	}
	
	@Autowired
	@Qualifier("vendedorServiceImpl")
	VendedorService vendedorService;
	
	@GetMapping("/vendedores")
	public List<VendedorResourse> obtenerVendedores() {
		
		List<VendedorResourse> listado = new ArrayList<>();
		vendedorService.obtenerVendedores().forEach(vendedor -> {
			VendedorResourse vendedorResourse = new ModelMapper().map(vendedor, VendedorResourse.class);
			listado.add(vendedorResourse);	
		});
		return listado;
	}
	
	@GetMapping("/vendedores/{id}")
	public VendedorResourse obtenerVendedorPorId(@PathVariable("id") long id) throws Exception {
		Vendedor vendedor = vendedorService.obtenerVendedorPorId(id);
		VendedorResourse vendedorResourse = new ModelMapper().map(vendedor, VendedorResourse.class);
		return vendedorResourse;
	}
	
	@PostMapping("/vendedores")
	public ResponseEntity<VendedorResourse> guardarVendedor(@RequestBody VendedorReducidoResourse request) throws Exception {	
		//modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.LOOSE); //Para poder asignador solamente coindicencias
		Vendedor vendedor = new ModelMapper().map(request, Vendedor.class);	//Setear valores a Vendedor	
		Vendedor vendedorNuevo = vendedorService.insertarVendedor(vendedor); //Insertar Vendedor		
		VendedorResourse vendedorResourse = new ModelMapper().map(vendedorNuevo, VendedorResourse.class); //Setear valores a VendedorResource
		return new ResponseEntity<>(vendedorResourse, HttpStatus.CREATED);
	}
	
	@PostMapping("/vendedorProducto")
	public ResponseEntity<VendedorProductoResourse> guardarVendedorProducto(@RequestBody VendedorProductoReducidoResourse request) throws Exception {	
		
		VendedorProducto vendedorProducto = new VendedorProducto();
		
		//OBTENER PRODUCTO
		Producto producto = vendedorService.obtenerProductoPorId(request.getIdproducto());
		vendedorProducto.setProducto(producto);
		
		//OBTENER VENDEDOR
		Vendedor vendedor = vendedorService.obtenerVendedorPorId(request.getIdvendedor());
		vendedorProducto.setVendedor(vendedor);
		
		vendedorProducto.setPrecio(request.getPrecio());
		VendedorProducto vendedorProductoNuevo = vendedorService.insertarVendedorProducto(vendedorProducto);
		VendedorProductoResourse vendedorProductoResourse = 
				new ModelMapper().map(vendedorProductoNuevo, VendedorProductoResourse.class);
		return new ResponseEntity<>(vendedorProductoResourse, HttpStatus.CREATED);
	}
	
	@GetMapping("/vendedorProducto")
	public List<VendedorProductoResourse> obtenerVendedorProducto() {
		
		List<VendedorProductoResourse> listado = new ArrayList<>();
		vendedorService.obtenerVendedorProducto().forEach(vendedorProducto -> {
			VendedorProductoResourse vendedorProductoResourse = new VendedorProductoResourse();
			vendedorProductoResourse.setIdVendedorProducto(vendedorProducto.getIdVendedorProducto());
			
			//PRODUCTO
			Producto producto = new ModelMapper().map(vendedorProducto, Producto.class);

			//VENDEDOR
			Vendedor vendedor = new ModelMapper().map(vendedorProducto.getVendedor(), Vendedor.class);
			
			vendedorProductoResourse.setProducto(producto);
			vendedorProductoResourse.setVendedor(vendedor);
			vendedorProductoResourse.setPrecio(vendedorProducto.getPrecio());
			listado.add(vendedorProductoResourse);	
		});
		return listado;
	}
	
	@GetMapping("/vendedorProducto/{id}")
	public VendedorProductoResourse obtenerVendedorProductoPorId(@PathVariable("id") long id) throws Exception {
		VendedorProducto vendedorProducto = vendedorService.obtenerVendedorProductoPorId(id);
		VendedorProductoResourse vendedorProductoResourse = new VendedorProductoResourse();
		vendedorProductoResourse.setIdVendedorProducto(vendedorProducto.getIdVendedorProducto());
		
		//PRODUCTO
		Producto producto = new Producto ();
		producto.setIdProducto(vendedorProducto.getProducto().getIdProducto());
		producto.setNombre(vendedorProducto.getProducto().getNombre());
		producto.setDescripcion(vendedorProducto.getProducto().getDescripcion());
		producto.setUnidadMedida(vendedorProducto.getProducto().getUnidadMedida());
		//VENDEDOR
		Vendedor vendedor = new Vendedor();
		vendedor.setIdVendedor(vendedorProducto.getVendedor().getIdVendedor());
		vendedor.setNombres(vendedorProducto.getVendedor().getNombres());
		vendedor.setApellidos(vendedorProducto.getVendedor().getApellidos());
		vendedor.setCargo(vendedorProducto.getVendedor().getCargo());
		
		vendedorProductoResourse.setProducto(producto);
		vendedorProductoResourse.setVendedor(vendedor);
		vendedorProductoResourse.setPrecio(vendedorProducto.getPrecio());
		return vendedorProductoResourse;
	}
}
