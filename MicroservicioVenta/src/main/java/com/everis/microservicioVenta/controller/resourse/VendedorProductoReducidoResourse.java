package com.everis.microservicioVenta.controller.resourse;

import java.math.BigDecimal;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class VendedorProductoReducidoResourse {
	
	private Long idvendedor;
	private Long idproducto;
	private BigDecimal precio;
}
